package com.tecnotab.frontend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.tecnotab.frontend.dao.UserRepository;
import com.tecnotab.frontend.model.User;

@SpringBootApplication
public class UserBackendApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(UserBackendApplication.class, args);
	}

	@Autowired
	UserRepository userRepo;
	
	@Override
	public void run(String... args) throws Exception {
		
		userRepo.save(new User("Yadhwik", "Ananniya", "tomcruise@gmail.com"));
		userRepo.save(new User("Srinivas", "Pitchubabu", "msdhoni132@gmail.com"));
		userRepo.save(new User("Savin", "Surfer", "sophiaRobo@hotmail.com"));
		userRepo.save(new User("Harry", "Potter", "rummy123@hotmail.com"));
	}

}
